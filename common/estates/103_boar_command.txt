
estate_boar_command = {
	icon = 1
	color = { 127 21 0 }
	
	trigger = {
		tag = R62
	}
	
	country_modifier_happy = {
		global_manpower = 10	# +10k flat manpower
		prestige_from_land = 1	# +100% prestige from land battles
		infantry_power = 0.10	# +10% infantry power
	}
	
	country_modifier_neutral = {
		global_manpower = 10	# +10k flat manpower
	}
	
	country_modifier_angry = {
		global_unrest = 1	# +1 national unrest
	}
	
	land_ownership_modifier = {
		boar_command_loyalty_modifier = 0.2	# +20% loyalty equilibrium, scale with land ownership
	}
	
	province_independence_weight = {
		factor = 1
		modifier = {
			factor = 1.5
			culture_group = owner
			religion_group = owner
		}
		modifier = {
			factor = 1.5
			base_manpower = 5
		}
		modifier = {
			factor = 0.75
			development = 20
		}
		modifier = {
			factor = 0.5
			NOT = { is_state_core = owner }
		}
	}
	
	base_influence = 10.0
	
	influence_modifier = {
		desc = "Influence Modifier: "
		trigger = {
			#<triggers>
		}
		influence = 10.0
	}
	
	loyalty_modifier = {
		desc = "Loyalty Modifier: "
		trigger = {
			#<triggers>
		}
		loyalty = 10.0
	}
	
	contributes_to_curia_treasury = no
	
	privileges = {
		estate_boar_command_land_rights
		estate_boar_command_military_thinking
		estate_boar_command_x
		estate_boar_command_iron_drills
		estate_boar_command_exaltations_from_valour
		estate_boar_command_management
		estate_boar_command_ninyu_kikun_nosunin
	}
	
	agendas = {
		estate_boar_command_campaign_xiadao
		estate_boar_command_hire_advisor
		estate_boar_command_fire_advisor
		estate_boar_command_crush_revolts
		estate_boar_command_build_an_army
		estate_boar_command_bigger_army_than_rival
	}
	
	influence_from_dev_modifier = 1.0
}