
country_decisions = {
	
	
	racial_pop_menu = {
		major = yes
		potential = {

		}
		allow = {
			<$races_all$ 
			custom_trigger_tooltip = {
				tooltip = tolerance_of_$race_adj$_info_tooltip
				always = yes
			}			
			$>

			hidden_trigger = {
				OR = {
					NOT = { has_country_flag = racial_pop_menu_opened }
					had_country_flag = {	#If something weird happens and the flag wasn't cleared, allow to open after a year
						flag = racial_military_menu_opened
						days = 365
					}
				}
			}
		}
		effect = {
			custom_tooltip = pop_menu_access_tt
			hidden_effect = {
				if = { limit = { ai = yes }
					country_event = { id = racial_pop_misc.1 }
				}
				else = {
					country_event = { id = racial_pop_misc.3 }
				}
				set_country_flag = racial_pop_menu_opened
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				OR = {
					has_country_modifier = racial_pop_menu_cooldown
					NOT = { has_low_tolerance_of_any_race_trigger = yes }
					NOT = { adm_power = 100 }
					# NOT = { stability = 2 }
				}
			}
		}
	}
	
	racial_military_menu = {
		major = yes
		potential = {
			ai = no
		}
		allow = {
			hidden_trigger = {
				NOT = { has_country_flag = racial_military_menu_opened }
			}
			NOT = { has_country_modifier = restructuring_military }
			#mil_power = 200 #Allow you to see % without the nessecary military power
		}
		effect = {
			custom_tooltip = mil_menu_access_tt
			hidden_effect = {
				country_event = { id = racial_modifiers.1 }
				set_country_flag = racial_military_menu_opened
			}
		}
		ai_will_do = { #The AI can't use this anyway
			factor = 0
			#modifier = {
			#	factor = 0
			#	has_low_tolerance_of_any_race_trigger = yes
			#}
			#modifier = {
			#	factor = 0
			#	NOT = { mil_power = 300 }
			#}
			#modifier = {
			#	factor = 0
			#	NOT = { stability = 3 }
			#}
		}
	}
}
